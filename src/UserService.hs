{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

module UserService (run) where

import Data.List (isInfixOf)
import Data.Char (toLower)
import Data.Aeson (ToJSON, FromJSON)
import GHC.Generics
import Web.Scotty
import Network.Wai.Middleware.Cors

data User = User { userId  :: Int,
                   name    :: String,
                   country :: String
                 } deriving (Show, Generic)

data Response = Response { users      :: [User],    -- list of users
                           page       :: Int,       -- requested page
                           pageSize   :: Int,       -- number of users on a full page
                           firstPage  :: Bool,      -- is the first page?
                           lastPage   :: Bool,      -- is the last page?
                           totalPages :: Int,       -- total number of pages
                           total      :: Int        -- total number of users
                          } deriving (Show, Generic)

instance ToJSON   User
instance FromJSON User
instance ToJSON   Response
instance FromJSON Response

allUsers = [
  User {userId = 1,  name = "Jimmy",     country = "United Kingdom"},
  User {userId = 2,  name = "Anojan",    country = "Sri Lanka"},
  User {userId = 3,  name = "Frank",     country = "Germany"},
  User {userId = 4,  name = "Zbigniew",  country = "Poland"},
  User {userId = 5,  name = "Ninna",     country = "Sweden" },
  User {userId = 6,  name = "Pooran",    country = "India"},
  User {userId = 7,  name = "Tianning",  country = "China" },
  User {userId = 8,  name = "Roman",     country = "Ukraine"},
  User {userId = 9,  name = "Aram",      country = "Armenia"},
  User {userId = 10, name = "Sven",      country = "Germany"},
  User {userId = 11, name = "Mike",      country = "Germany"},
  User {userId = 12, name = "Andrey",    country = "Russia"},
  User {userId = 13, name = "Mohamed",   country = "Egypt"},
  User {userId = 14, name = "Heinrich",  country = "Germany"},
  User {userId = 15, name = "Khalil",    country = "Lebanon"},
  User {userId = 16, name = "Sebastian", country = "Germany"},
  User {userId = 17, name = "Michael",   country = "Germany"},
  User {userId = 18, name = "Mohamed",   country = "Egypt"},
  User {userId = 19, name = "Dragan",    country = "Serbia"}]

isSubStringOf :: String -> String -> Bool
isSubStringOf sub str = [ toLower c | c <- sub ] `isInfixOf` [ toLower c | c <- str ]

selectPage :: Int -> Int -> [User] -> [User]
selectPage page pageSize = take pageSize . drop (pageSize * page)

queryUser :: String -> [User] -> [User]
queryUser query = filter (\u -> query `isSubStringOf` (name u) || query `isSubStringOf` (country u))

createResponse :: Int -> Int -> String -> Response
createResponse p ps q = let queriedUser = queryUser q allUsers
                            pagedUser = selectPage p  ps queriedUser
                            total = length queriedUser
                            totalPages = 1 + (total - 1) `div` ps
                        in  Response {users = pagedUser, page = p, pageSize = ps, firstPage = (p == 0),
                            lastPage = (p >= totalPages - 1), totalPages = totalPages, total = total}

run :: IO ()
run = do
        putStrLn "Starting server"
        scotty 4000 $ do
              middleware simpleCors
              get "/users" $ do
                    page <- param "page"
                    pageSize <- param "pageSize"
                    query <- param "query"
                    json $ createResponse page pageSize query

